module "cloudinit" {
  source             = "./modules/cloudinit/"
  cloudinit_name        = "centos8_cloudinit.iso"
  cloudinit_filename = "cloud_init_resize.cfg"
}

module "centos" {
  source = "./modules/centos/"
}

resource "libvirt_volume" "control_os_disk" {
  name           = "control_os_disk-${count.index}"
  base_volume_id = module.centos.centos_8_stream_id
  count          = var.control_count 
  pool = "nvme"
  format = "qcow2"
  size = "128849018880"
}

resource "libvirt_volume" "compute_os_disk" {
  name           = "compute_os_disk-${count.index}"
  base_volume_id = module.centos.centos_8_stream_id
  count          = var.control_count 
  pool = "nvme"
  format = "qcow2"
  size = "128849018880"
}

resource "libvirt_volume" "master_os_disk" {
  name           = "master_os_disk-${count.index}"
  base_volume_id = module.centos.centos_8_stream_id
  count          = var.control_count 
  pool = "nvme"
  format = "qcow2"
  size = "128849018880"
}

resource "libvirt_volume" "nova_disk" {
  name           = "nova_disk-${count.index}"
  count          = var.compute_count
  pool = "nvme"
  format = "qcow2"
  size = "322122547200"
}

resource "libvirt_volume" "ceph_disk" {
  name           = "ceph_disk-${count.index}"
  count          = var.compute_count
  pool = "sshd"
  format = "qcow2"
  size = "1073741824000"
}

// master vms
resource "libvirt_domain" "master" {
  name      = "master-${count.index}"
  memory    = "8192"
  vcpu      = 2
  count     = var.master_count
  cpu = {
    mode = "host-passthrough"
  }
  autostart = true

  network_interface {
    network_name = "br0"
    hostname = "master-${count.index}.${var.dns_domain}"
    mac = "52:54:00:5e:28:e${count.index}"
  }
  // OS image
  disk {
    volume_id = element(libvirt_volume.master_os_disk.*.id, count.index)
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport    = true
  }

  cloudinit = module.cloudinit.cloudinit_id

provisioner "file" {
  source      =var.script_path
  destination = "/home/luser/bootstrap.sh"

    connection {
      type                = "ssh"
      user                = var.ssh_username
      timeout             = "2m"
      private_key         = file(var.ssh_private_key)
      host                = "192.168.1.30" # the libvirt prosionner does not know how to get IP addr from custom network
    }
}

provisioner "remote-exec" {
  inline = [
    "chmod +x /home/luser/bootstrap.sh",
    "/home/luser/bootstrap.sh \"192.168.1.30\" master-${count.index}.${var.dns_domain}",
  ]
    connection {
      type                = "ssh"
      user                = var.ssh_username
      timeout             = "2m"
      private_key         = file(var.ssh_private_key)
      host                = "192.168.1.30" # the libvirt prosionner does not know how to get IP addr from custom network
    }
}
}

// control vms
resource "libvirt_domain" "control" {
  name      = "control-${count.index}"
  memory    = "8192"
  vcpu      = 2
  count     = var.control_count
  cpu = {
    mode = "host-passthrough"
  }
  autostart = true

  network_interface {
    network_name = "br0"
    hostname = "control-${count.index}.${var.dns_domain}"
    mac = "52:54:00:5b:b7:9${count.index}"
  }
  // OS image
  disk {
    volume_id = element(libvirt_volume.control_os_disk.*.id, count.index)
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport    = true
  }

  cloudinit = module.cloudinit.cloudinit_id

provisioner "file" {
  source      =var.script_path
  destination = "/home/luser/bootstrap.sh"

    connection {
      type                = "ssh"
      user                = var.ssh_username
      timeout             = "2m"
      private_key         = file(var.ssh_private_key)
      host                = "192.168.1.3${count.index +1}" # the libvirt prosionner does not know how to get IP addr from custom network
    }
}

provisioner "remote-exec" {
  inline = [
    "chmod +x /home/luser/bootstrap.sh",
    "/home/luser/bootstrap.sh \"192.168.1.3${count.index +1}\" control-${count.index}.${var.dns_domain}",
  ]
    connection {
      type                = "ssh"
      user                = var.ssh_username
      timeout             = "2m"
      private_key         = file(var.ssh_private_key)
      host                = "192.168.1.3${count.index +1}" # the libvirt prosionner does not know how to get IP addr from custom network
    }
}
}

// control vms
resource "libvirt_domain" "compute" {
  name      = "compute-${count.index}"
  memory    = "20480"
  vcpu      = 8
  count     = var.compute_count
  cpu = {
    mode = "host-passthrough"
  }
  autostart = true

  network_interface {
    network_name = "br0"
    hostname = "compute-${count.index}.${var.dns_domain}"
    mac = "52:54:00:3e:ee:4${count.index}"
  }
  // OS image
  disk {
    volume_id = element(libvirt_volume.compute_os_disk.*.id, count.index)
  }

  disk {
    volume_id = element(libvirt_volume.nova_disk.*.id, count.index)
  }

  disk {
    volume_id = element(libvirt_volume.ceph_disk.*.id, count.index)
  }


  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport    = true
  }

  cloudinit = module.cloudinit.cloudinit_id

provisioner "file" {
  source      =var.script_path
  destination = "/home/luser/bootstrap.sh"

    connection {
      type                = "ssh"
      user                = var.ssh_username
      timeout             = "2m"
      private_key         = file(var.ssh_private_key)
      host                = "192.168.1.3${count.index +4}" # the libvirt prosionner does not know how to get IP addr from custom network
    }
}

provisioner "remote-exec" {
  inline = [
    "chmod +x /home/luser/bootstrap.sh",
    "/home/luser/bootstrap.sh \"192.168.1.3${count.index +4}\" compute-${count.index}.${var.dns_domain}",
  ]
    connection {
      type                = "ssh"
      user                = var.ssh_username
      timeout             = "2m"
      private_key         = file(var.ssh_private_key)
      host                = "192.168.1.3${count.index +4}" # the libvirt prosionner does not know how to get IP addr from custom network
    }
}

}