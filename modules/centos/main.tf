terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.3"
    }
  }
}

resource "libvirt_volume" "centos_7" {
  name   = "centos_7"
  source = "${var.images_dir}/CentOS-7-x86_64-GenericCloud.qcow2"
  pool   = var.pool
}

resource "libvirt_volume" "centos_8" {
  name   = "centos_8"
  source = "${var.images_dir}/CentOS-8-GenericCloud-8.2.x86_64.qcow2"
  pool   = var.pool
}

resource "libvirt_volume" "centos_8_stream" {
  name   = "centos_8_stream"
  source = "${var.images_dir}/CentOS-Stream-GenericCloud-8.x86_64.qcow2"
  pool   = var.pool
}
