
output "centos_7_id" {
  value = libvirt_volume.centos_7.id
}

output "centos_8_id" {
  value = libvirt_volume.centos_8.id
}

output "centos_8_stream_id" {
  value = libvirt_volume.centos_8_stream.id
}