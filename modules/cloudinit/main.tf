terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.3"
    }
  }
}

data "template_file" "user_data" {
  template = file("${path.module}/${var.cloudinit_filename}")
}

resource "libvirt_cloudinit_disk" "cloudinit" {
  name      = var.cloudinit_name
  user_data = data.template_file.user_data.rendered
  pool      = var.pool
}
