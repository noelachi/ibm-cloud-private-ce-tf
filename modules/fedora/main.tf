terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.3"
    }
  }
}

resource "libvirt_volume" "fedora_33" {
  name   = "fedora_33"
  source = "${var.images_dir}/Fedora-Cloud-Base-33-1.2.x86_64.qcow2"
  pool   = var.pool
}

resource "libvirt_volume" "fedora_32" {
  name   = "fedora_32"
  source = "${var.images_dir}/Fedora-Cloud-Base-32-1.6.x86_64.qcow2"
  pool   = var.pool
}
