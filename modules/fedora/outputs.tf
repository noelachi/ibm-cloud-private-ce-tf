
output "fedora_33_id" {
  value = libvirt_volume.fedora_33.id
}

output "fedora_32_id" {
  value = libvirt_volume.fedora_32.id
}