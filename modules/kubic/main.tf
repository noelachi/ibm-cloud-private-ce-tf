terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.3"
    }
  }
}

resource "libvirt_volume" "kubic" {
  name   = "kubic"
  source = "${var.images_dir}/openSUSE-MicroOS.x86_64-Kubic-kubeadm-kvm-and-xen.qcow2"
  pool   = var.pool
}
