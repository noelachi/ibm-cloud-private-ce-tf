variable "pool" {
  description = "libvirt storage pool name for VM disks"
  default     = "default"
}

variable "images_dir" {
  description = "Local image repository"
  default     = "/home/luser/labs/images"
}