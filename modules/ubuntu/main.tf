terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.3"
    }
  }
}

resource "libvirt_volume" "ubuntu_18" {
  name   = "ubuntu_18"
  source = "${var.images_dir}/bionic-server-cloudimg-amd64.img"
  pool   = var.pool
}

resource "libvirt_volume" "ubuntu_16" {
  name   = "ubuntu_16"
  source = "${var.images_dir}/xenial-server-cloudimg-amd64-disk1.img"
  pool   = var.pool
}

resource "libvirt_volume" "ubuntu_20" {
  name   = "ubuntu_20"
  source = "${var.images_dir}/ubuntu-20.04-server-cloudimg-amd64-disk-kvm.img"
  pool   = var.pool
}
