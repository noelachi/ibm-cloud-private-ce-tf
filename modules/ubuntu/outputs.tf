output "ubuntu_18_id" {
  value = libvirt_volume.ubuntu_18.id
}

output "ubuntu_16_id" {
  value = libvirt_volume.ubuntu_16.id
}

output "ubuntu_20_id" {
  value = libvirt_volume.ubuntu_20.id
}