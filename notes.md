# Notes from Kubic
sudo chown -R apache: /var/www/html/
sudo chmod -R 755 /var/www/html

Create VMs

```bash
virt-install \
--name master-0 \
--memory 16384 \
--vcpus 4 \
--disk pool=nvme,size=120,readonly=false  \
--cdrom /var/lib/libvirt/images/openSUSE-Kubic-DVD-x86_64-Current.iso \
--os-variant opensusetumbleweed \
--network network=br0,model=virtio,mac=52:54:00:5e:28:e0 \
--network network=vmbr01,model=virtio  \
--network network=vmbr02,model=virtio \
--cpu host-passthrough --hvm --vnc &

virsh vol-create-as --pool nvme --name cmp0-120g1 120G --format qcow2  --allocation 0
virsh vol-create-as --pool nvme --name cmp0-340g1 340G --format qcow2  --allocation 0
for i in {1..3}; do virsh vol-create-as --pool sshd --name cmp0-360g$i 360G --format qcow2 --allocation 0; done
virt-install \
--name compute-0 \
--memory 24576 \
--vcpus 10 \
--disk vol=nvme/cmp0-120g1  \
--disk vol=nvme/cmp0-340g1  \
--disk vol=sshd/cmp0-360g1 \
--disk vol=sshd/cmp0-360g2  \
--disk vol=sshd/cmp0-360g3  \
--cdrom /var/lib/libvirt/images/openSUSE-Kubic-DVD-x86_64-Current.iso \
--os-variant opensusetumbleweed \
--network network=br0,model=virtio,mac=52:54:00:3e:ee:40 \
--network network=vmbr01,model=virtio \
--network network=vmbr02,model=virtio \
--cpu host-passthrough --hvm --vnc  &


virsh vol-create-as --pool nvme --name cmp1-120g1 120G --format qcow2  --allocation 0
virsh vol-create-as --pool nvme --name cmp1-340g1 340G --format qcow2  --allocation 0
for i in {1..3}; do virsh vol-create-as --pool sshd --name cmp1-360g$i 360G --format qcow2 --allocation 0; done
virt-install \
--name compute-1 \
--memory 24576 \
--vcpus 10 \
--disk vol=nvme/cmp1-120g1  \
--disk vol=nvme/cmp1-340g1  \
--disk vol=sshd/cmp1-360g1 \
--disk vol=sshd/cmp1-360g2  \
--disk vol=sshd/cmp1-360g3  \
--cdrom /var/lib/libvirt/images/openSUSE-Kubic-DVD-x86_64-Current.iso \
--os-variant opensusetumbleweed \
--network network=br0,model=virtio,mac=52:54:00:3e:ee:41 \
--network network=vmbr01,model=virtio \
--network network=vmbr02,model=virtio \
--cpu host-passthrough --hvm --vnc &

virsh vol-create-as --pool nvme --name cmp2-120g1 120G --format qcow2  --allocation 0
virsh vol-create-as --pool nvme --name cmp2-340g1 340G --format qcow2  --allocation 0
for i in {1..3}; do virsh vol-create-as --pool sshd --name cmp2-360g$i 360G --format qcow2 --allocation 0; done
virt-install \
--name compute-2 \
--memory 24576 \
--vcpus 10 \
--disk vol=nvme/cmp2-120g1  \
--disk vol=nvme/cmp2-340g1  \
--disk vol=sshd/cmp2-360g1 \
--disk vol=sshd/cmp2-360g2  \
--disk vol=sshd/cmp2-360g3  \
--cdrom /var/lib/libvirt/images/openSUSE-Kubic-DVD-x86_64-Current.iso \
--os-variant opensusetumbleweed \
--network network=br0,model=virtio,mac=52:54:00:3e:ee:42 \
--network network=vmbr01,model=virtio \
--network network=vmbr02,model=virtio \
--cpu host-passthrough --hvm --vnc &

cat <<EOF | sudo tee /etc/sysconfig/network/ifcfg-enp1s0
BOOTPROTO='static'
STARTMODE='auto'
IPADDR='192.168.1.36/24'
MTU='0'
EOF

cat <<EOF | sudo tee /etc/sysconfig/network/ifcfg-eth1
BOOTPROTO='static'
STARTMODE='auto'
IPADDR='192.168.2.30/24'
MTU='0'
EOF

cat <<EOF | sudo tee /etc/sysconfig/network/ifcfg-eth2
BOOTPROTO='static'
STARTMODE='auto'
IPADDR='192.168.3.30/24'
MTU='0'
EOF

cat <<EOF | sudo tee /etc/sysconfig/network/ifroute-enp1s0
default 192.168.1.254 - enp1s0
EOF

sed -i 's/NETCONFIG_DNS_POLICY="auto"/NETCONFIG_DNS_POLICY=""/g' /etc/sysconfig/network/config /etc/sysconfig/network/config
sed -i 's/NETCONFIG_DNS_STATIC_SEARCHLIST=""/NETCONFIG_DNS_STATIC_SEARCHLIST="homelab.nac"/g' /etc/sysconfig/network/config /etc/sysconfig/network/config
sed -i 's/NETCONFIG_DNS_STATIC_SERVERS=""/NETCONFIG_DNS_STATIC_SERVERS="192.168.1.5"/g' /etc/sysconfig/network/config /etc/sysconfig/network/config

hostnamectl set-hostname compute-2.homelab.nac
```

# Deploy Kubernetes

## Deploy Kubernetes
https://linoxide.com/containers/install-kubernetes-on-ubuntu/
https://tech.osci.kr/2020/08/03/97477266/

1. Initialise master

```bash
sudo kubeadm init --apiserver-advertise-address=192.168.1.30 --pod-network-cidr=10.244.0.0/16
```

2. Configure network plugin

```bash
#calico
Edit network range before running kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml
https://docs.projectcalico.org/getting-started/kubernetes/quickstart

#canal
https://docs.projectcalico.org/getting-started/kubernetes/flannel/flannel
```

3. Join master and taint compute nodes

```bash
kubeadm join 192.168.1.30:6443 --token bokjiw.fdi18c21nafh5bu9 \
    --discovery-token-ca-cert-hash sha256:925a737d226d40e6fa122a5e63087ed8defdfe3c36a5a2721e8cb880a45f8dc6
```

4. Install CertManager 
https://cert-manager.io/docs/installation/kubernetes/
https://docs.cert-manager.io/en/release-0.8/tasks/issuers/setup-selfsigned.html

```bash
kubectl create namespace cert-manager
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.1.0/cert-manager.yaml

kube-0:~ # cat certmanager.yaml (cluster issuer does not work, using namespace issuer)
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: selfsigning-issuer
spec:
  selfSigned: {}

kubectl apply -f certmanager.yaml

using with ingresses https://cert-manager.io/docs/usage/ingress/
```

5. Metallb

The official dock works very well.
https://metallb.universe.tf/installation/


6. Install nginx ingress controller

The bitnami version works well with metallb while the one provided by nginx does not.

https://bitnami.com/stack/nginx-ingress-controller/helm

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
kubectl create namespace ingresses
helm install nginx-ing01 bitnami/nginx-ingress-controller -n ingresses
```

7. Install ceph
Have to define storage crush
To define storage pools, follow https://github.com/rook/rook/blob/master/Documentation/ceph-pool-crd.md
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-ceph-cluster-within-kubernetes-using-rook

```bash
cd $HOME/labs/rook/cluster/examples/kubernetes/ceph
kubectl create -f crds.yaml -f common.yaml -f operator.yaml
kubectl create -f cluster.yaml
kubectl apply -f dashboard-loadbalancer.yaml

#admin password
kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o jsonpath="{['data']['password']}" | base64 --decode && echo
"0vU!4a;]G?JmBA7$s>"!""
#change storage classes
kubectl apply -f toolbox.yaml
kubectl -n rook-ceph exec -it $(kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[0].metadata.name}') bash
ceph osd tree
ceph osd crush rm-device-class 1
ceph osd crush set-device-class nvme 1

#create storage pools
see provisioning/ceph/pools.yaml

#create storage classes
see provisioning/ceph/storageclasses.yaml

# set default storage class
kubectl get storageclass
kubectl patch storageclass hdd-standalone -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

6. Taint and label nodes

```bash
kubectl taint node compute-0.homelab.nac role=compute:NoSchedule
kubectl taint node compute-1.homelab.nac role=compute:NoSchedule
kubectl taint node compute-2.homelab.nac role=compute:NoSchedule

#remove 
kubectl taint node compute-0.homelab.nac role=compute:NoSchedule-
kubectl taint node compute-1.homelab.nac role=compute:NoSchedule-
kubectl taint node compute-2.homelab.nac role=compute:NoSchedule-

kubectl label node  kube-cy4-kube002 openstack-control-plane=enabled

kubectl label node master-0 role=master
kubectl label node compute-0 role=compute
kubectl label node compute-1 role=compute
kubectl label node compute-2 role=compute

kubectl label node control-0.homelab.nac role=control
kubectl label node control-1.homelab.nac role=control
kubectl label node control-2.homelab.nac role=control

#to remove
kubectl taint node compute-0.homelab.nac node.kubernetes.io/compute:NoSchedule-
kubectl taint node compute-1.homelab.nac node.kubernetes.io/compute:NoSchedule-
kubectl taint node compute-2.homelab.nac node.kubernetes.io/compute:NoSchedule-
```

To schedule pods on that node, do

```bash
annotations:
  scheduler.alpha.kubernetes.io/tolerations: '[{"key":"species","value":"compute"}]'
```

7. Kubernetes Dashboard
https://artifacthub.io/packages/helm/k8s-dashboard/kubernetes-dashboard

```bash
helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
kubectl create namespace kube-dashboard 
helm install kube-dashboard kubernetes-dashboard/kubernetes-dashboard --set service.type=LoadBalancer -n kube-dashboard --set service.loadBalancerIP=192
.168.1.63

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-dashboard
EOF

cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-dashboard
EOF

kubectl -n kube-dashboard describe secret $(kubectl -n kube-dashboard get secret | grep admin-user | awk '{print $1}')
```
8. Deploy Monitoring (posponed)
https://docs.bitnami.com/tutorials/integrate-logging-kubernetes-kibana-elasticsearch-fluentd/

# Deploy OpenStack

https://tech.osci.kr/2020/08/03/97477266/
https://kubernetes.github.io/ingress-nginx/user-guide/multiple-ingress/
https://docs.openstack.org/install-guide/environment-sql-database-rdo.html

 1. Prepare
- Create openstack namespaces
- Create ingress controller that listen to openstack namespace only

2. Deploy Monitoring (posponed)
https://docs.bitnami.com/tutorials/integrate-logging-kubernetes-kibana-elasticsearch-fluentd/
https://medium.com/@thulasya/deploy-elasticsearch-on-kubernetes-via-helm-in-google-kubernetes-cluster-da722f3a8883
https://www.digitalocean.com/community/tutorials/how-to-set-up-an-elasticsearch-fluentd-and-kibana-efk-logging-stack-on-kubernetes

2.1. Install elasticsearch
```bash
kubectl create namespace monitoring
helm install -n  monitoring elasticsearch bitnami/elasticsearch --set master.nodeSelector."role"=master,master.replicas=1,coordinating.nodeSelector."role"=master,data.nodeSelector."role"=compute,master.persistence.size=40Gi,data.persistence.size=220Gi,curator.enabled=true,coordinating.replicas=1,curator.nodeSelector."role"=master,metrics.nodeSelector."role"=master

Elastic
```bash
helm install -n  logging elasticsearch elastic/elasticsearch --set nodeSelector."role"=compute --set volumeClaimTemplate.resources.requests.storage=220Gi --set rbac.create=true
```
Kibana
```
helm -n logging install kibana elastic/kibana --set service.type=LoadBalancer,nodeSelector."role"=master

```
Fluent-bit 
```bash
helm repo add fluent https://fluent.github.io/helm-charts
helm install -n  logging  fluent-bit fluent/fluent-bit -f values.yaml

kubectl apply -n logging -f  fluent-bit-service-account.yaml -f fluent-bit-role.yaml -f fluent-bit-role-binding.yaml
kubectl apply -n logging -f fluent-bit-configmap.yaml
kubectl apply -n logging -f fluent-bit-ds.yaml
```

`Last good doc`
https://docs.opstreelabs.in/logging-operator/

3. Install galera cluster 
https://docs.bitnami.com/kubernetes/infrastructure/mariadb-galera/

```bash
helm install -n  openstack galera bitnami/mariadb-galera --set fullnameOverride=galera --set rootUser.password=shroot --set galera.mariabackup.password=shroot --set nodeSelector."role"=compute --set persistence.storageClass=nvme-standalone --set persistence.size=80Gi 

#installation notes
NAME: galera
LAST DEPLOYED: Sat Dec 26 09:38:20 2020
NAMESPACE: openstack
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **

Tip:

  Watch the deployment status using the command:

    kubectl get sts -w --namespace openstack -l app.kubernetes.io/instance=galera

MariaDB can be accessed via port "3306" on the following DNS name from within your cluster:

    galera.openstack.svc.cluster.local

To obtain the password for the MariaDB admin user run the following command:

    echo "$(kubectl get secret --namespace openstack galera -o jsonpath="{.data.mariadb-root-password}" | base64 --decode)"

To connect to your database run the following command:

    kubectl run galera-client --rm --tty -i --restart='Never' --namespace openstack --image docker.io/bitnami/mariadb-galera:10.5.8-debian-10-r36 --command \
      -- mysql -h galera -P 3306 -uroot -p$(kubectl get secret --namespace openstack galera -o jsonpath="{.data.mariadb-root-password}" | base64 --decode) my_database

To connect to your database from outside the cluster execute the following commands:

    kubectl port-forward --namespace openstack svc/galera 3306:3306 &
    mysql -h 127.0.0.1 -P 3306 -uroot -p$(kubectl get secret --namespace openstack galera -o jsonpath="{.data.mariadb-root-password}" | base64 --decode) my_database

To upgrade this helm chart:

    helm upgrade --namespace openstack galera bitnami/mariadb-galera \
      --set rootUser.password=$(kubectl get secret galera -o jsonpath="{.data.mariadb-root-password}" | base64 --decode) \
      --set db.name=my_database \
      --set galera.mariabackup.password=$(kubectl get secret --namespace openstack galera -o jsonpath="{.data.mariadb-galera-mariabackup-password}" | base64 --decode)
```

# LB IP addresses
192.168.1.61:443 kube-dashboard
192.168.1.62:8443 ceph-dashboard

4. Install rabbitmq
https://github.com/bitnami/charts/tree/master/bitnami/rabbitmq/#installing-the-chart

```bash
helm install -n  openstack rabbitmq bitnami/rabbitmq --set auth.username=openstack,auth.password=openstack1,auth.erlangCookie=secretcookie,nodeSelector."role"=compute, persistence.storageClass=nvme-standalone,persistence.size=20Gi,replicaCount=3,service.type=LoadBalancer

NAME: rabbitmq
LAST DEPLOYED: Sat Dec 26 09:49:56 2020
NAMESPACE: openstack
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **

Credentials:

    echo "Username      : openstack"
    echo "Password      : $(kubectl get secret --namespace openstack rabbitmq -o jsonpath="{.data.rabbitmq-password}" | base64 --decode)"
    echo "ErLang Cookie : $(kubectl get secret --namespace openstack rabbitmq -o jsonpath="{.data.rabbitmq-erlang-cookie}" | base64 --decode)"

RabbitMQ can be accessed within the cluster on port  at rabbitmq.openstack.svc.

To access for outside the cluster, perform the following steps:

Obtain the LoadBalancer IP:

NOTE: It may take a few minutes for the LoadBalancer IP to be available.
      Watch the status with: 'kubectl get svc --namespace openstack -w rabbitmq'

    export SERVICE_IP=$(kubectl get svc --namespace openstack rabbitmq --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}")

To Access the RabbitMQ AMQP port:

    echo "URL : amqp://$SERVICE_IP:5672/"

To Access the RabbitMQ Management interface:

    echo "URL : http://$SERVICE_IP:15672/"
```

5. memcached

```bash
helm install -n  openstack memcached bitnami/memcached --set memcachedUsername=openstack,memcachedPassword=openstack1,nodeSelector."role"=compute,persistence.storageClass=nvme-standalone,replicaCount=3,architecture="high-availability"

NAME: memcached
LAST DEPLOYED: Sat Dec 26 09:54:26 2020
NAMESPACE: openstack
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **
Memcached endpoints are exposed on the headless service named: memcached.
Please see https://github.com/memcached/memcached/wiki/ConfiguringClient to understand the Memcached model and need for client-based consistent hashing.
You might also want to consider more advanced routing/replication approaches with mcrouter: https://github.com/facebook/mcrouter/wiki/Replicated-pools-setup
```

6. etcd (looks like not required in kubernetes)

7. Keystone
Doc: https://github.com/openstack/openstack-helm/tree/master/keystone/templates
 7.1. Install the container (see Dockerfile)
 7.2. Create config maps
 secret --> env variable --> config map