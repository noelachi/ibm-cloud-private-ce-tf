#!/bin/sh

set -e
#set -x
OS=CentOS_8
CRIO_VERSION=1.19
KUBE_VERSION=1.19.5-0

sudo dnf -y update 
sudo modprobe overlay
sudo modprobe br_netfilter

sudo dnf -y install iproute-tc iscsi-initiator-utils curl lvm2 qemu-guest-agent yum-utils device-mapper-persistent-data

# Set up required sysctl params, these persist across reboots.
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
vm.max_map_count=262144
EOF

cat <<EOF | sudo tee /etc/modules-load.d/br_netfilter.conf
overlay
br_netfilter
EOF

sudo sysctl --system

sudo setenforce 0
sudo sed -i 's/enforcing/disabled/g' /etc/selinux/config /etc/selinux/config

#sudo dnf -y install 'dnf-command(copr)'
#sudo dnf -y copr enable rhcontainerbot/container-selinux
sudo curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable.repo https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/CentOS_8/devel:kubic:libcontainers:stable.repo
sudo curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable:cri-o:${CRIO_VERSION}.repo https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:${CRIO_VERSION}/CentOS_8/devel:kubic:libcontainers:stable:cri-o:${CRIO_VERSION}.repo
sudo yum -y install cri-o

sudo sed -i 's@conmon = \"\"@conmon = \"/usr/bin/conmon\"@g' /etc/crio/crio.conf

new_string="registries = [\n\"quay.io\",\n\"docker.io\",\n]"
sudo sed -i "s@#registries =@$new_string@" /etc/crio/crio.conf
sudo sed -i "s@\] \[@\]@g" /etc/crio/crio.conf

sudo systemctl enable --now crio

#static IP
sudo nmcli connection modify "System eth0" ipv4.addresses "${1}/24"
sudo nmcli connection modify "System eth0" ipv4.gateway "192.168.1.254"
sudo nmcli connection modify "System eth0" ipv4.dns "192.168.1.5"
sudo nmcli connection modify "System eth0" ipv4.dns-search "homelab.nac"
sudo nmcli connection modify "System eth0" ipv4.method manual

sudo hostnamectl set-hostname $2

cat <<EOF | sudo tee /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
$1	$2
EOF

cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
sudo yum install -y  kubelet-$KUBE_VERSION kubeadm-$KUBE_VERSION kubectl-$KUBE_VERSION --disableexcludes=kubernetes
#sudo yum install -y  kubelet kubeadm kubectl --disableexcludes=kubernetes

sudo mkdir /var/lib/kubelet
cat <<EOF | sudo tee /var/lib/kubelet/config.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
EOF

cat <<EOF | sudo tee /etc/sysconfig/kubelet
KUBELET_EXTRA_ARGS=--container-runtime=remote --cgroup-driver=systemd --container-runtime-endpoint="unix:///var/run/crio/crio.sock"
EOF

sudo systemctl enable --now kubelet

sudo systemctl reboot