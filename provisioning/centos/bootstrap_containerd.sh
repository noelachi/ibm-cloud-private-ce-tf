#!/bin/sh

set -e
#set -x
OS=CentOS_8
CRIO_VERSION=1.19
KUBE_VERSION=1.19.5-0

sudo dnf -y update 
sudo modprobe overlay
sudo modprobe br_netfilter

sudo dnf -y install iproute-tc iscsi-initiator-utils

# Set up required sysctl params, these persist across reboots.
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

cat <<EOF | sudo tee /etc/modules-load.d/br_netfilter.conf
overlay
br_netfilter
EOF

sudo sysctl --system

sudo setenforce 0
sudo sed -i 's/enforcing/disabled/g' /etc/selinux/config /etc/selinux/config

sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum update -y && sudo yum install -y containerd.io
sudo mkdir -p /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml
sudo systemctl restart containerd

#static IP
sudo nmcli connection modify "System eth0" ipv4.addresses "${1}/24"
sudo nmcli connection modify "System eth0" ipv4.gateway "192.168.1.254"
sudo nmcli connection modify "System eth0" ipv4.dns "192.168.1.5"
sudo nmcli connection modify "System eth0" ipv4.dns-search "homelab.nac"
sudo nmcli connection modify "System eth0" ipv4.method manual

sudo hostnamectl set-hostname $2

cat <<EOF | sudo tee /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
$1	$2
EOF

cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
sudo yum install -y  kubelet-$KUBE_VERSION kubeadm-$KUBE_VERSION kubectl-$KUBE_VERSION --disableexcludes=kubernetes
#sudo yum install -y  kubelet kubeadm kubectl --disableexcludes=kubernetes

sudo mkdir /var/lib/kubelet
cat <<EOF | sudo tee /var/lib/kubelet/config.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
EOF

sudo systemctl enable --now kubelet

sudo systemctl reboot