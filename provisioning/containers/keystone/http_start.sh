#!/bin/bash

# Assume the service runs on top of Apache httpd when user is root.
if [[ "$(whoami)" == 'root' ]]; then
    rm -rf /var/run/httpd/* /run/httpd/* /tmp/httpd*

    # CentOS 8 has an issue with mod_ssl which produces an invalid Apache
    # configuration in /etc/httpd/conf.d/ssl.conf. This causes the following error
    # on startup:
    #   SSLCertificateFile: file '/etc/pki/tls/certs/localhost.crt' does not exist or is empty
    # Work around this by generating certificates manually.
    if  [[ ! -e /etc/pki/tls/certs/localhost.crt ]]; then
        /usr/libexec/httpd-ssl-gencerts
    fi
fi
echo "ServerName 127.0.0.1" >> /etc/httpd/conf/httpd.conf
exec /usr/sbin/apachectl -DFOREGROUND