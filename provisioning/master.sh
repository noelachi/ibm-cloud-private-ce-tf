#!/bin/sh

UBUNTU_VERSION="ubuntu-xenial"
DOCKER_VERSION="5:18.09.7~3-0~"

set -e

sudo apt update 
sudo apt -y upgrade
sudo apt -y install apt-transport-https ca-certificates curl software-properties-common lvm2 qemu-guest-agent python
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce=${DOCKER_VERSION}${UBUNTU_VERSION}
sudo apt-mark hold docker-ce
sudo usermod -aG docker ${USER}
sudo systemctl enable docker

cat <<EOF | sudo tee /etc/network/interfaces
auto eth0
iface eth0 inet static
	address $1
	netmask 255.255.255.0
	gateway 192.168.1.254
	dns-nameservers 192.168.1.5
	dns-search homelab.nac
EOF

sudo hostnamectl set-hostname $2

cat <<EOF | sudo tee -a /etc/hosts
$1	$2
EOF
sudo systemctl reboot