#!/bin/sh

set -e
set -x
OS=xUbuntu_20.04
CRIO_VERSION=1.20
KUBE_VERSION=1.19.5-0

sudo apt update 
sudo apt -y upgrade
sudo apt -y install iproute2 curl lvm2 qemu-guest-agent nfs-common apt-transport-https ca-certificates curl software-properties-common lvm2 open-iscsi ntp

sudo modprobe overlay
sudo modprobe br_netfilter

# Set up required sysctl params, these persist across reboots.
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
vm.max_map_count=262144
EOF

cat <<EOF | sudo tee /etc/modules-load.d/br_netfilter.conf
overlay
br_netfilter
EOF

sudo sysctl --system

echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/ /" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
echo "deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/$CRIO_VERSION/$OS/ /" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:$CRIO_VERSION.list

curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$CRIO_VERSION/$OS/Release.key | sudo apt-key add -
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/Release.key | sudo  apt-key add -

sudo apt-get update
sudo apt-get -y install cri-o cri-o-runc
sudo sed -i 's@conmon = \"\"@conmon = \"/usr/bin/conmon\"@g' /etc/crio/crio.conf

new_string="registries = [\n\"quay.io\",\n\"docker.io\",\n]"
sudo sed -i "s@#registries =@$new_string@" /etc/crio/crio.conf
sudo sed -i "s@\] \[@\]@g" /etc/crio/crio.conf

#static IP
sudo rm -rf /etc/netplan/*
cat <<EOF | sudo tee /etc/netplan/50-netplan.yaml
network:
    version: 2
    renderer: networkd
    ethernets:
        ens3:
            addresses:
                - $1/24
            gateway4: 192.168.1.254
            nameservers:
                search: [homelab.nac]
                addresses: [192.168.1.5]
EOF


sudo hostnamectl set-hostname $2
sudo systemctl disable apparmor

cat <<EOF | sudo tee /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
$1	$2
EOF

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

sudo mkdir /var/lib/kubelet
cat <<EOF | sudo tee /var/lib/kubelet/config.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
EOF

cat <<EOF | sudo tee /etc/default/kubelet
KUBELET_EXTRA_ARGS=--feature-gates="AllAlpha=false,RunAsGroup=true" --container-runtime=remote --cgroup-driver=systemd --container-runtime-endpoint='unix:///var/run/crio/crio.sock' --runtime-request-timeout=5m
EOF

sudo systemctl enable --now crio
sudo systemctl enable --now kubelet

sudo systemctl reboot