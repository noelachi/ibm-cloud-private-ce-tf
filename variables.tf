variable "dns_domain" {
  description = "DNS domain name"
  default     = "homelab.nac"

}

variable "master_count" {
  description = "number of virtual-machine of same type that will be created"
  default     = 1
}

variable "control_count" {
  description = "number of virtual-machine of same type that will be created"
  default     = 3
}

variable "compute_count" {
  description = "number of virtual-machine of same type that will be created"
  default     = 3
}

variable "memory" {
  description = "The amount of RAM (MB) for a node"
  default     = 8192
}

variable "vcpu" {
  description = "The amount of virtual CPUs for a node"
  default     = 2
}

variable "ssh_username" {
  description = "SSH username"
  default     = "luser"
}

variable "ssh_private_key" {
  description = "the private key to use"
  default     = "~/.ssh/id_ed25519"
}

variable "script_path" {
  description = "Script that installs docker and prepare things for kubernetes"
  default     = "/home/luser/labs/ibm-pcloud-ce/provisioning/centos/bootstrap.sh"
}